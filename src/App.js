import NavBar from "./Components/NavBar";
import React, {useState, useEffect} from "react";
import Images from '../src/Components/Images'
import "././Components/cinema.css"
import axios from "axios";



const App =()=>{
  
  const [movies, setMovies] = useState([]);
  const [search, setSearch] = useState("kiss");
  
  const searchHandler =(e)=>{
    setSearch(e.target.value)
    
  }
  
  const fetchMovies = async ()=>{
    const URL = 'https://www.omdbapi.com/?s=' + search + '&apikey=c2062f5b'
    const response = await fetch(URL)
    const finaldata = await response.json()
    setMovies(finaldata.Search)
    console.log(search)
  };
  

  useEffect(()=>{
    
    const filterMovie = setTimeout(() => {
      fetchMovies();
    }, 500);
    return()=>{
      clearTimeout(filterMovie);
    }
  }, [search]);

  useEffect(()=>{
    fetchMovies();
  }, [])

  return(
   <div>
      <NavBar/>
      <div className="cinema w-full  flex md:justify-start justify-center ">
        <p className="font-semibold text-white lg:text-[42px] md:text-[32px] text-[25px] sm:ml-[50px] md:my-[10%] my-[8%] md:text-start text-center w-[250px] h-fit">Watch something incredible.</p>
      </div>

      <div className="w-[95%] m-auto mt-[30px]">
        <form>
          <label className="flex font-normal text-[14px] border-[#000000]">Search</label>
          <input type="text" id="search" value={search} onChange={searchHandler} className="border-[1.5px] text-black px-[5px] border-[#000000] h-[35px] w-[100%]"/>
        
        </form>
        </div>

        <div className=" w-[98%] mb-[30px] ml-auto">

          <div className="mt-[30px] overflow-x-scroll bg-scroll flex">
            {movies && movies.map((movie, index) =>
                                
              <div key = {movie.index}  className="my-[20px] flex">
                
                <div className=" w-[200px] h-[200px] mx-[7px]">
                  <img src={movie.Poster} alt={movie.Title} className="w-full h-full rounded-lg" />
                </div>
                      
              </div>
            )}  
          </div>
          </div>
   </div>
  )
}

export default App