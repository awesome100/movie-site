import React from 'react'

function NavBar() {
    return (
    <div className='bg-[#292929] h-[100px] w-full my-auto items-center  flex '>
        <p className='border p-[10px] w-fit  text-white font-mono sm:ml-[20px] sm:m-0 m-auto'> MyTestApp</p>
    </div>
    )
}

export default NavBar
